<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');

Route::get('/team/name', function() {
	return Auth::user()->currentTeam->name;
});

Route::get('/metrics', function() {
	for ($i = 0; $i < 100; $i++) {
		DB::table('performance_indicators')->insert([
			'monthly_recurring_revenue' => mt_rand(1000, 2000),
			'yearly_recurring_revenue' => mt_rand (5000, 60000),
			'daily_volume' => mt_rand(100, 300),
			'new_users' => mt_rand(50, 100),
			'created_at' => Carbon\Carbon::now()->subDays($i),
			'updated_at' => Carbon\Carbon::now()->subDays($i),
			]);
	}
	return 'Done!';
});