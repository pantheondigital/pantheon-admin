<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */


    /**
     * Define repeated check - integrates w/ envoyer and forge
     */

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                  ->everyTenMinutes()
                  ->thenPing('http://beats.envoyer.io/heartbeat/1zSwafqmu7A7hrY');

         $schedule->command('spark:kpi')
                  ->daily();         
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
